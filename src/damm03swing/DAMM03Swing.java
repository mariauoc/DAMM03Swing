/*
 * Ejemplo con Interfaz de usuario (GUI)
 */
package damm03swing;

/**
 *
 * @author profesores.linkia
 */
public class DAMM03Swing {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Creamos nuestra ventana
        Principal ventanaPrincipal = new Principal();
        // La situamos centrada en la pantalla
        ventanaPrincipal.setLocationRelativeTo(null);
        // La hacemos visible
        ventanaPrincipal.setVisible(true);
        Persona p  = new Persona("Enric");
        System.out.println(p.getId()+" "+p.getNombre());
        Persona otra = new Persona("Manolo");
        System.out.println(otra.getId()+" "+otra.getNombre());
    }
    
}
