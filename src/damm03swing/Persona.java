/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package damm03swing;

/**
 *
 * @author profesores.linkia
 */
public class Persona {

    private int id;
    private String nombre;
    private static int contador = 0;

    public Persona(String nombre) {
        this.nombre = nombre;
        id = contador;
        contador++;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
